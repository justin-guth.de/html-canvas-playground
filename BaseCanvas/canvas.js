class Vector2 {

    constructor(x, y) {

        this.x = x;
        this.y = y;
    }

    static fromUnitCircle() {

        let theta = Math.random() * 2 * Math.PI;
        return new Vector2(Math.cos(theta), Math.sin(theta));
    }

    normalised() {

        let mag = this.magnitude();
        return new Vector2(this.x / mag, this.y / mag);
    }

    dot(other) {

        return this.x * other.x + this.y * other.y;
    }

    inv() {
        return new Vector2(-this.x, -this.y);
    }

    add(other) {

        return new Vector2(this.x + other.x, this.y + other.y);
    }

    increment(other) {

        this.x += other.x;
        this.y += other.y;
    }

    times(n) {

        return new Vector2(this.x * n, this.y * n);
    }

    magnitude() {

        return Math.sqrt(this.x * this.x + this.y * this.y);
    }

    toString() {

        return "(" + this.x + ", " + this.y + ")";
    }

    orthoUnit() {

        return new Vector2(this.y, - this.x);
    }

    clone() {

        return new Vector2(this.x, this.y);
    }
}

var canvas = null;
var context = null;

var last = new Date();
var lastSpawnTime = 0;

function run() {

    let now = new Date();
    let delta = now - last;
    delta /= 1000;
    last = now;





    window.requestAnimationFrame(run)
}

function init() {


}


function scaleCanvasToParent() {

    if (canvas == null) {
        console.error("Canvas has not been set yet.");
    }

    canvas.width = canvas.parentNode.clientWidth;
    canvas.height = canvas.parentNode.clientHeight;

}

window.addEventListener("load", () => {
    canvas = document.getElementById("canvas");
    context = canvas.getContext("2d");
    scaleCanvasToParent();

    init();
    run();
})

window.addEventListener("resize", () => {
    scaleCanvasToParent();
})