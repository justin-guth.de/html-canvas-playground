var canvas = null;
var context = null;

var last = new Date();
var lastSpawnTime = 0;

class Vector2 {

    constructor(x, y) {

        this.x = x;
        this.y = y;
    }

    static fromUnitCircle() {

        let theta = Math.random() * 2 * Math.PI;
        return new Vector2(Math.cos(theta), Math.sin(theta));
    }

    normalised() {

        let mag = this.magnitude();
        return new Vector2(this.x / mag, this.y / mag);
    }

    dot(other) {

        return this.x * other.x + this.y * other.y;
    }

    inv() {
        return new Vector2(-this.x, -this.y);
    }

    add(other) {

        return new Vector2(this.x + other.x, this.y + other.y);
    }

    increment(other) {

        this.x += other.x;
        this.y += other.y;
    }

    times(n) {

        return new Vector2(this.x * n, this.y * n);
    }

    magnitude() {

        return Math.sqrt(this.x * this.x + this.y * this.y);
    }

    toString() {

        return "(" + this.x + ", " + this.y + ")";
    }

    orthoUnit() {

        return new Vector2(this.y, - this.x);
    }

    clone() {

        return new Vector2(this.x, this.y);
    }

    distanceTo(other) {

        return (other.add(this.inv()).magnitude());
    }
}

class Sphere {

    constructor(position, trajectory) {

        this.position = position;
        this.trajectory = trajectory;
        this.setRadius(20, 60);

        this.setOscillation(3, 30)
        this.oscillationPosition = 0.0;
        this.setSpeed(100);
        this.setColor("black")
    }

    setRadius(r, rTarget) {
        this.radius = r;
        this.targetRadius = rTarget;
    }

    setOscillation(freq, ampl) {
        this.oscillationFrequencyFactor = freq;
        this.oscillationAamplitudeFactor = ampl;

    }

    setSpeed(speed) {
        this.speed = speed;
    }
    setColor(color) {
        this.color = color;
    }

    step(delta) {

        this.oscillationPosition += delta;
        this.position.increment(this.trajectory.times(delta * this.speed));
    }

    paint(canvas, mousePosition, growDistance) {

        let distanceToMouse = this.position.distanceTo(mousePosition);

        let radius = this.radius;

        if (distanceToMouse < growDistance - this.radius) {

            let ratio = distanceToMouse / growDistance;
            let sig = 1 / (1 + Math.exp(-10 * (-ratio + 0.5)));

            radius += sig * (this.targetRadius - radius);
        }

        let context = canvas.getContext("2d");
        let position = this.position.add(
            this.trajectory.orthoUnit().times(
                Math.sin(
                    this.oscillationFrequencyFactor * this.oscillationPosition
                ) * this.oscillationAamplitudeFactor
            )
        );

        context.fillStyle = this.color;
        context.beginPath();
        context.arc(position.x, position.y, radius, 0, 2 * Math.PI);
        context.fill();

    }

    isDead() {

        return this.position.y > canvas.height + this.radius + 10;
    }
}

class Sphere1 extends Sphere {

    constructor(position, trajectory) {
        super(position, trajectory);

        this.setOscillation(3, 30);
        this.setRadius(6, 40);
        this.setSpeed(80);
        this.setColor("#FF2244");
    }
}

class Sphere2 extends Sphere {

    constructor(position, trajectory) {
        super(position, trajectory);

        this.setOscillation(2, 20);
        this.setRadius(3, 16);
        this.setSpeed(100);
        this.setColor("#FFEEEE");
    }
}

class Sphere3 extends Sphere {

    constructor(position, trajectory) {
        super(position, trajectory);

        this.setOscillation(0.7, 40);
        this.setRadius(16, 60);
        this.setSpeed(50);
        this.setColor("#106060");
    }
}

var sphereSpawnDirection1 = new Vector2(-0.3, 1.0).normalised();
var sphereSpawnDirection2 = new Vector2(-0.4, 1.0).normalised();
var sphereSpawnDirection3 = new Vector2(-0.7, 1.0).normalised();
var spheres1 = [];
var spheres2 = [];
var spheres3 = [];
var spawnableWidth1;
var spawnableWidth2;
var spawnableWidth3;
var spawnBase1;
var spawnBase2;
var spawnBase3;
var rows1;
var rows2;
var rows3;


var mouse = new Vector2(-1000, -1000);

function run() {

    let now = new Date();
    let delta = now - last;
    delta /= 1000;
    last = now;

    context.clearRect(0, 0, canvas.width, canvas.height);

    newSpheres1 = [];
    newSpheres2 = [];
    newSpheres3 = [];

    spheres3.forEach((s) => {

        s.step(delta);
        s.paint(canvas, mouse, 250);

        if (!s.isDead()) {
            newSpheres3.push(s);
        }
    });

    spheres1.forEach((s) => {

        s.step(delta);
        s.paint(canvas, mouse, 250);

        if (!s.isDead()) {
            newSpheres1.push(s);
        }
    });

    spheres2.forEach((s) => {

        s.step(delta);
        s.paint(canvas, mouse, 250);

        if (!s.isDead()) {
            newSpheres2.push(s);
        }
    });



    spheres1 = newSpheres1;
    spheres2 = newSpheres2;
    spheres3 = newSpheres3;




    window.requestAnimationFrame(run)
}

function spawnSpheres1() {

    for (let row = 0; row < rows1; row++) {

        let x = spawnBase1 + row * spawnableWidth1 / (rows1 - 1);
        let y = -30;
        spheres1.push(new Sphere1(new Vector2(x, y), sphereSpawnDirection1.clone()));
    }

}
function spawnSpheres2() {

    for (let row = 0; row < rows2; row++) {

        let x = spawnBase2 + row * spawnableWidth2 / (rows2 - 1);
        let y = -30;
        spheres2.push(new Sphere2(new Vector2(x, y), sphereSpawnDirection2.clone()));
    }

}

function spawnSpheres3() {

    for (let row = 0; row < rows3; row++) {

        let x = spawnBase3 + row * spawnableWidth3 / (rows3 - 1);
        let y = -70;
        spheres3.push(new Sphere3(new Vector2(x, y), sphereSpawnDirection3.clone()));
    }

}

function init() {

    scaleCanvasToParent();

    setInterval(spawnSpheres1, 1000);
    setInterval(spawnSpheres2, 300);
    setInterval(spawnSpheres3, 3000);

    window.addEventListener("click", (ev) => {

        spheres.push(new Sphere(new Vector2(ev.x, ev.y), Vector2.fromUnitCircle()));
    });

    window.addEventListener("mousemove", (ev) => {

        mouse.x = ev.x;
        mouse.y = ev.y;
    })
}


function scaleCanvasToParent() {

    canvas.width = canvas.parentNode.clientWidth;
    canvas.height = canvas.parentNode.clientHeight;


    spawnableWidth1 = canvas.width + Math.abs((sphereSpawnDirection1.x * canvas.height));
    spawnableWidth2 = canvas.width + Math.abs((sphereSpawnDirection2.x * canvas.height));
    spawnableWidth3 = canvas.width + Math.abs((sphereSpawnDirection2.x * canvas.height));
    spawnBase1 = 0;
    spawnBase2 = 0;
    spawnBase3 = 0;
    if (sphereSpawnDirection1.x > 0) {
        spawnBase1 -= sphereSpawnDirection1 * canvas.height;
    }

    if (sphereSpawnDirection2.x > 0) {
        spawnBase2 -= sphereSpawnDirection2 * canvas.height;
    }
    if (sphereSpawnDirection3.x > 0) {
        spawnBase3 -= sphereSpawnDirection3 * canvas.height;
    }

    if (canvas == null) {
        console.error("Canvas has not been set yet.");
    }




    rows1 = Math.floor((canvas.width / 2000) * 17);
    rows2 = Math.floor((canvas.width / 2000) * 29);
    rows3 = Math.floor((canvas.width / 2000) * 13);

}

window.addEventListener("load", () => {
    canvas = document.getElementById("canvas");
    context = canvas.getContext("2d");
    scaleCanvasToParent();

    init();
    run();
})

window.addEventListener("resize", () => {
    scaleCanvasToParent();
})