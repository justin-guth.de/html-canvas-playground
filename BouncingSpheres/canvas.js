var canvas = null;
var context = null;

function run() {

    baseSpeed = 200;
    randomSpeedFactor = 200;
    fadeFactor = 25;
    randomFadeFactor = 25;

    function Sphere(x, y) {

        let theta = Math.random() * 2 * Math.PI;

        let scale = (baseSpeed + (Math.random() * randomSpeedFactor));

        this.dx = Math.cos(theta) * scale;
        this.dy = Math.sin(theta) * scale;


        this.r = 20 + Math.random() * 30;
        this.x = x;
        this.y = y;
        this.fade = 100;
        this.fadeSpeed = fadeFactor + Math.random() * randomFadeFactor;


        this.newColor = function () {

            let colorCode = (Math.floor(Math.random() * 0x1000000)).toString(16);
            
            colorCode = "0".repeat(6 - colorCode.length) + colorCode;
            
            this.color = '#' + colorCode + "00";
        }

        this.applyFadeColor = function () {

            if (this.fade > 0) {

                let alpha = Math.floor((this.fade / 100) * 0xFF).toString(16);

                alpha = "0".repeat(2 - alpha.length) + alpha;

                this.color = (this.color.substring(0, 7)) + alpha;
            }
        }

        this.newColor();

        this.step = function (delta, maxWidth, maxHeight) {

            this.fade -= delta * this.fadeSpeed;
            this.applyFadeColor();

            this.x += delta * this.dx;
            this.y += delta * this.dy;


            if ((this.x + this.r > maxWidth && this.dx > 0)
                || (this.x - this.r < 0 && this.dx < 0)) {
                this.dx = - this.dx
                this.newColor();
                this.applyFadeColor();
            }

            if ((this.y + this.r > maxHeight && this.dy > 0)
                || (this.y - this.r < 0 && this.dy < 0)) {
                this.dy = - this.dy
                this.newColor();
                this.applyFadeColor();
            }

        }
    }

    var spheres = [];

    var timeDelta = 0.0;
    var lastTime = new Date();

    function animate() {

        newTime = new Date();
        timeDelta = newTime - lastTime;
        timeDelta /= 1000
        lastTime = newTime;

        requestAnimationFrame(animate);

        context.clearRect(0, 0, canvas.width, canvas.height);

        context.fillStyle = "#363642";
        context.fillRect(0,0,canvas.width, canvas.height);

        spheres.forEach((sphere, ind) => {

            sphere.step(timeDelta, canvas.width, canvas.height);

            context.beginPath();
            context.arc(sphere.x, sphere.y, sphere.r, 0, 2 * Math.PI, false);
            context.fillStyle = sphere.color;
            context.fill();

        });

        spheres = spheres.filter((sphere) => { return sphere.fade > 0; });

    }

    animate();

    function newSphere(e) {
        spheres.push(
            new Sphere(
                e.pageX,
                e.pageY)
        );
    }

    var mouseDown = false;
    window.addEventListener("mousedown", (e) => {
        mouseDown = true;
        newSphere(e);
    });

    window.addEventListener("mouseup", () => {
        mouseDown = false;
    });

    

    window.addEventListener("mousemove", (e) => {

        if (!mouseDown) {
            return;
        }

        newSphere(e);
    });
}


function scaleCanvasToParent() {

    if (canvas == null) {
        console.error("Canvas has not been set yet.");
    }

    canvas.width = canvas.parentNode.clientWidth;
    canvas.height = canvas.parentNode.clientHeight;

}

window.addEventListener("load", () => {
    canvas = document.getElementById("canvas");
    context = canvas.getContext("2d");
    scaleCanvasToParent();

    run();
})

window.addEventListener("resize", () => {
    scaleCanvasToParent();
})