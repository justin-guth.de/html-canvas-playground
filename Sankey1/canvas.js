var canvas = null;
var context = null;

function Sankey(data, categories, canvas,
    spaceAroundCategory = 30,
    categoryWidth = 15,
    paddingHorizontal = 40,
    paddingVertical = 40) {

    this.data = JSON.parse(JSON.stringify(data));
    this.categories = JSON.parse(JSON.stringify(categories));
    this.spaceAroundCategory = spaceAroundCategory;
    this.categoryWidth = categoryWidth;
    this.paddingHorizontal = paddingHorizontal;
    this.paddingVertical = paddingVertical;
    this.textMargin = 15;
    this.textColor = "#000000";
    this.alphaSuffix = "80"

    this.canvas = canvas;
    this.context = canvas.getContext("2d");


    this.getRandomColor = function () {
        let colorCode = (Math.floor(Math.random() * 0x1000000)).toString(16);

        colorCode = "0".repeat(6 - colorCode.length) + colorCode;

        return '#' + colorCode;
    }

    this.centerColor = this.getRandomColor();


    this.paint = function () {

        var canvasHeight = this.canvas.height;
        var canvasWidth = this.canvas.width;

        var paddedWidth = canvasWidth - 2 * this.paddingHorizontal;
        var paddedHeight = canvasHeight - 2 * this.paddingVertical;

        var widthPerLayer = (paddedWidth - this.categoryWidth) / (this.inputLayers.length + this.outputLayers.length);

        var lowestPixelPerAmount = null;

        for (let i = 0; i < 2; i++) {
            layer = [this.inputLayers, this.outputLayers][i];

            for (let j = 0; j < layer.length; j++) {

                var padding = (layer[j].category_count - 1) * spaceAroundCategory;

                var leftoverPixels = paddedHeight - padding;

                var pixelPerAmount = leftoverPixels / layer[j].total_amount;

                if (lowestPixelPerAmount == null || pixelPerAmount < lowestPixelPerAmount) {
                    lowestPixelPerAmount = pixelPerAmount;
                }

            }
        }

        // paint inputLayers

        for (let i = 0; i < this.inputLayers.length; i++) {

            var layer = this.inputLayers[this.inputLayers.length - i - 1];
            var y = this.paddingVertical;

            layer.categories.forEach(
                (pair, index) => {
                    let category = pair[1];
                    var height = category.total_amount * lowestPixelPerAmount;
                    var width = this.categoryWidth;
                    var x = this.paddingVertical + i * widthPerLayer;

                    var textY = y + height / 2;
                    var textX = x + this.categoryWidth + this.textMargin;

                    category._draw = {
                        x: x,
                        y: y,
                        height: height,
                        used_height: 0
                    };

                    this.context.fillStyle = category.color;
                    this.context.fillRect(x, y, width, height);
                    this.context.fillStyle = this.textColor;
                    this.context.font = '16px sans-serif';
                    this.context.fillText(category.name, textX, textY);
                    y += height + this.spaceAroundCategory;
                    ;
                }
            )
        }

        // paint center layer

        var y = this.paddingVertical;

        var x = this.paddingVertical + this.inputLayers.length * widthPerLayer;


        var height = this.outputLayers[0].total_amount * lowestPixelPerAmount;

        var textY = y + height / 2;
        var textX = x + this.categoryWidth + this.textMargin;

        this._center = {
            _draw: {
                x: x,
                y: y,
                height: height,
                used_height: 0
            }
        };


        var width = this.categoryWidth;
        this.context.fillStyle = this.centerColor;
        this.context.fillRect(x, y, width, height);
        this.context.fillStyle = this.textColor;
        this.context.font = '16px sans-serif';
        this.context.fillText("All", textX, textY);

        // paint output layer
        // I know it's bad :/

        for (let i = 0; i < this.outputLayers.length; i++) {

            var layer = this.outputLayers[i];
            var y = this.paddingVertical;

            layer.categories.forEach(
                (pair, index) => {
                    let category = pair[1];
                    var height = category.total_amount * lowestPixelPerAmount;
                    var width = this.categoryWidth;
                    var x = this.paddingVertical + (1 + i + this.inputLayers.length) * widthPerLayer;

                    var textY = y + height / 2;
                    this.context.font = '16px sans-serif';
                    var textX = x - this.categoryWidth - this.textMargin - this.context.measureText(category.name).width;

                    category._draw = {
                        x: x,
                        y: y,
                        height: height,
                        used_height: 0
                    };

                    this.context.fillStyle = category.color;
                    this.context.fillRect(x, y, width, height);
                    this.context.fillStyle = this.textColor;
                    this.context.fillText(category.name, textX, textY);
                    y += height + this.spaceAroundCategory;
                    ;
                }
            )
        }

        this.inputLayers.forEach((layer) => {

            layer.categories.forEach((pair) => {

                var category = pair[1];

                var x = category._draw.x + this.categoryWidth;
                var y = category._draw.y;

                var connectedCategory = null;

                if (category.parent == null) {

                    connectedCategory = this._center;

                }
                else {

                    connectedCategory = this.categories[category.parent];
                }

                x2 = connectedCategory._draw.x;
                y2 = (connectedCategory._draw.y + connectedCategory._draw.used_height);

                xmid = (x2 + x) / 2;

                this.context.beginPath();
                this.context.moveTo(x, y);
                this.context.bezierCurveTo(xmid, y, xmid, y2, x2, y2);
                this.context.lineTo(x2, y2 + category._draw.height);

                this.context.bezierCurveTo(xmid, y2 + category._draw.height, xmid, y + category._draw.height, x, y + category._draw.height);
                this.context.closePath();
                this.context.fillStyle = category.color + this.alphaSuffix;
                this.context.fill();

                connectedCategory._draw.used_height += category._draw.height;
            })
        });

        this._center._draw.used_height = 0;

        this.outputLayers.forEach((layer) => {

            layer.categories.forEach((pair) => {

                var category = pair[1];

                var x = category._draw.x;
                var y = category._draw.y;

                var connectedCategory = null;

                if (category.parent == null) {

                    connectedCategory = this._center;

                }
                else {

                    connectedCategory = this.categories[category.parent];
                }

                x2 = connectedCategory._draw.x  + this.categoryWidth;
                y2 = (connectedCategory._draw.y + connectedCategory._draw.used_height);

                xmid = (x2 + x) / 2;

                this.context.beginPath();
                this.context.moveTo(x, y);
                this.context.bezierCurveTo(xmid, y, xmid, y2, x2, y2);
                this.context.lineTo(x2, y2 + category._draw.height);

                this.context.bezierCurveTo(xmid, y2 + category._draw.height, xmid, y + category._draw.height, x, y + category._draw.height);
                this.context.closePath();
                this.context.fillStyle = category.color + this.alphaSuffix;
                this.context.fill();

                connectedCategory._draw.used_height += category._draw.height;
            })
        });



    }

    console.log("Processing datasets:", this.data);

    // inititialise total_amount fields to 0

    for (key in this.categories) {

        this.categories[key]["total_amount"] = 0;
    }

    // sum all data amounts into their respective data categories.
    this.data.forEach((element, index) => {
        console.log("Processing:", element);

        let categoryIndex = element.category;

        this.categories[categoryIndex]["total_amount"] += element.amount;

    });


    // perform depths search in order to find sums for parent categories.
    // Also sets whether a category is a leaf and the categorys depth.

    var rootNodes = [];

    for (key in this.categories) {
        if (this.categories[key].parent == null) {
            rootNodes.push(key);
        }
    }

    console.log("Found root nodes:", rootNodes);

    var categories = this.categories;

    function depthSearch(key, previous = 0) {

        let depth = previous + 1;

        var children = Object.entries(categories).filter(
            (pair) => { return pair[1].parent == key; }
        ).map(
            (element) => { return element[0]; }
        );

        categories[key].depth = depth;

        categories[key].is_leaf_node = false;

        if (children.length == 0) {
            categories[key].is_leaf_node = true;
        }

        children.forEach(
            (element) => {
                categories[key].total_amount += depthSearch(element, depth);
            }
        );

        return categories[key].total_amount;
    }

    rootNodes.forEach((key) => {

        depthSearch(key);
    });

    this.inputDepth = 0;
    this.outputDepth = 0;

    for (key in this.categories) {

        var category = this.categories[key];

        // assign a color to each category:

        category.color = this.getRandomColor();

        if (category.type == "input" && category.depth > this.inputDepth) {
            this.inputDepth = category.depth;
        }
        else if (category.type == "output" && category.depth > this.outputDepth) {
            this.outputDepth = category.depth;
        }
    }

    // Now each category knows how much amount belongs to it as well as which depth it belongs to.
    // Now we need to compute the total amount per layer and the number of categories lying on that layer.

    this.inputLayers = [];
    this.outputLayers = [];

    var depthMaps = [[this.inputLayers, this.inputDepth, "input"],
    [this.outputLayers, this.outputDepth, "output"]];

    for (index in depthMaps) {

        pair = depthMaps[index];

        var layerMap = pair[0];
        var depth = pair[1];
        var type = pair[2];

        for (let i = 1; i < depth + 1; i++) {

            var layerInfo = {
                number: i,
                categories: [],
                total_amount: 0,
                category_count: 0
            }

            Object.entries(this.categories)
                .filter(
                    (element) => { return element[1].type == type && element[1].depth == i; }
                )
                .forEach(
                    (element, key) => {
                        layerInfo.categories.push([element[0], element[1]]);
                        layerInfo.total_amount += element[1].total_amount;
                        layerInfo.category_count += 1;
                    }
                );

            layerMap.push(layerInfo);
        }
    }


    console.log("Categories:", this.categories);
    console.log("Input layers:", this.inputLayers);
    console.log("Output layers:", this.outputLayers);

}

function run() {

    var data = {
        categories: {
            id_in: {
                name: "in",
                parent: null,
                type: "input"
            },
            id_out: {
                name: "out",
                parent: null,
                type: "output"
            },
            id_pc: {
                name: "paycheck",
                parent: "id_in",
                type: "input"
            },
            id_normalpc: {
                name: "normal paycheck",
                parent: "id_pc",
                type: "input"
            },
            id_bonus: {
                name: "bonus",
                parent: "id_pc",
                type: "input"
            },
            id_gifts: {
                name: "gifts",
                parent: "id_in",
                type: "input"
            },
            id_food: {
                name: "food",
                parent: "id_out",
                type: "output"
            },
            id_rent: {
                name: "rent",
                parent: "id_out",
                type: "output"
            },
            id_other: {
                name: "other",
                parent: "id_out",
                type: "output"
            }
        },

        datasets: [
            {
                title: "Restaurant",
                amount: 100,
                category: "id_food"
            },
            {
                title: "Restaurant",
                amount: 200,
                category: "id_food"
            },
            {
                title: "Rent November",
                amount: 1000,
                category: "id_rent"
            },
            {
                title: "Pay July",
                amount: 500,
                category: "id_normalpc"
            },
            {
                title: "Pay August",
                amount: 400,
                category: "id_normalpc"
            },
            {
                title: "Gift Grandma",
                amount: 1000,
                category: "id_gifts"
            },
            {
                title: "Other",
                amount: 700,
                category: "id_other"
            },
            {
                title: "X",
                amount: 100,
                category: "id_bonus"
            },

        ]
    };



    const s = new Sankey(data.datasets, data.categories, canvas);
    s.paint();

    window.addEventListener("resize", () => {
        s.paint();
    })
}


function scaleCanvasToParent() {

    if (canvas == null) {
        console.error("Canvas has not been set yet.");
    }

    canvas.width = canvas.parentNode.clientWidth;
    canvas.height = canvas.parentNode.clientHeight;

}

window.addEventListener("load", () => {
    canvas = document.getElementById("canvas");
    //context = canvas.getContext("2d");
    scaleCanvasToParent();

    run();
})

window.addEventListener("resize", () => {
    scaleCanvasToParent();
})