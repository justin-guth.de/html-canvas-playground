var canvas = null;
var context = null;

var last = new Date();

var volume = 0.5;

var audioSourcesA = [

    "./res/Akk1 C5.mp3",
    "./res/Akk1 C6.mp3",
    "./res/Akk1 E5.mp3",
    "./res/Akk1 E6.mp3",
    "./res/Akk1 F5.mp3",
    "./res/Akk1 G4.mp3",
    "./res/Akk1 G5.mp3",
    "./res/Akk1 G6.mp3"
];


var audioSourcesB = [

    "./res/Akk2 A4.mp3",
    "./res/Akk2 A5.mp3",
    "./res/Akk2 D5.mp3",
    "./res/Akk2 D6.mp3",
    "./res/Akk2 F5.mp3",
    "./res/Akk2 F6.mp3",
    "./res/Akk2 G5.mp3",
    "./res/Akk2 A6.mp3"
];

var audioSourcesC = [

    "./res/Akk3 A4.mp3",
    "./res/Akk3 A5.mp3",
    "./res/Akk3 B6.mp3",
    "./res/Akk3 C5.mp3",
    "./res/Akk3 C6.mp3",
    "./res/Akk3 E5.mp3",
    "./res/Akk3 E6.mp3",
    "./res/Akk3 B5.mp3"
];

var audioSourcesD = [

    "./res/Akk4 A6.mp3",
    "./res/Akk4 B4.mp3",
    "./res/Akk4 B5.mp3",
    "./res/Akk4 D5.mp3",
    "./res/Akk4 D6.mp3",
    "./res/Akk4 G5.mp3",
    "./res/Akk4 G6.mp3",
    "./res/Akk4 G4.mp3"
];

var audioSourcesList = [
    {
        source: audioSourcesA,
        color: "#99BBFF"
    },
    {
        source: audioSourcesB,
        color: "#ffc899"
    },
    {
        source: audioSourcesC,
        color: "#99ffa6"
    },
    {
        source: audioSourcesD,
        color: "#ff9999"
    }

];

var audioSourceIndex = 0;


function shuffleArray(arr) {
    arr.sort(() => Math.random() - 0.5);
}

function playTone(t) {

    let source = t.audio.source;
    //console.log(t.audio.source);

    shuffleArray(t.audio.source);

    for (let i = 0; i < 4; i++) {

        let notePath = t.audio.source[i];

        try {
            let audio = new Audio(notePath);
            audio.volume = volume;
            audio.play()
        } catch (error) {
            console.error(error);
        }
    }
}

class Block {

    constructor(color, audio) {

        this.color = color;
        this.audio = audio;
    }
}

class Row {
    static columns = 12;



    constructor() {

        this.erase = false;
        this.blocks = [];

        for (let col = 0; col < this.columns; col++) {

            this.blocks.push(null);
        }
    }

    addBlock(position, block) {

        this.blocks[position] = block;
    }

    isFull() {

        for (let i = 0; i < Row.columns; i++) {

            if (this.blocks[i] == null) {
                return false;
            }
        }

        return true;
    }

    isEmpty() {

        for (let i = 0; i < Row.columns; i++) {

            if (this.blocks[i] != null) {
                return false;
            }
        }

        return true;
    }

    clear() {
        this.erase = true;
    }

    finishClear() {
        this.erase = false;

        this.blocks = [];

        for (let col = 0; col < this.columns; col++) {

            this.blocks.push(null);
        }
    }

    applyTimeDelta(delta) {


    }

}

class Tetromino {

    constructor(x, y, field, color) {
        this.x = x;
        this.y = y;
        this.field = field;
        this.color = color;
        this.shape = this.getInitialShape();
        this.audio = audioSourcesList[Math.floor(Math.random() * audioSourcesList.length)];
    }

    rotateRight() {

        let newShape = []
        let shape = this.getShape();

        for (let i = 0; i < shape.length; i++) {
            for (let j = 0; j < shape[i].length; j++) {

                if (newShape[j] == null || newShape[j] == undefined) {
                    newShape[j] = []
                }

                newShape[j][shape.length - i - 1] = shape[i][j];
            }
        }

        this.setShape(newShape);
    }

    setShape(shape) {
        this.shape = shape;
    }

    getShape() {
        return this.shape;
    }

    hasNextStepCollision() {

        let nextY = this.y + 1;
        let shape = this.getShape();

        for (let i = 0; i < shape.length; i++) {
            for (let j = 0; j < shape[i].length; j++) {

                if (shape[i][j] == 0) {
                    continue;
                }

                if (this.field.isBlock(this.x + j, nextY + i)) {

                    return true;
                }
            }
        }

        return false;
    }

    hasLeftStepCollision() {

        let nextX = this.x - 1;
        let shape = this.getShape();

        for (let i = 0; i < shape.length; i++) {
            for (let j = 0; j < shape[i].length; j++) {

                if (shape[i][j] == 0) {
                    continue;
                }

                if (this.field.isBlock(nextX + j, this.y + i)) {

                    return true;
                }
            }
        }

        return false;
    }

    hasRightStepCollision() {

        let nextX = this.x + 1;
        let shape = this.getShape();

        for (let i = 0; i < shape.length; i++) {
            for (let j = 0; j < shape[i].length; j++) {

                if (shape[i][j] == 0) {
                    continue;
                }

                if (this.field.isBlock(nextX + j, this.y + i)) {

                    return true;
                }
            }
        }

        return false;
    }

    width() {

        let widest = 0;
        let shape = this.getShape();

        for (let i = 0; i < shape.length; i++) {

            widest = shape[i].length;
        }

        return widest;
    }

    getColor() {

        return this.color;
    }

    step(timeDelta) {

        if (this.hasNextStepCollision()) {

            let shape = this.getShape();

            playTone(this);

            for (let i = 0; i < shape.length; i++) {
                for (let j = 0; j < shape[i].length; j++) {

                    if (shape[i][j] == 0) {
                        continue;
                    }

                    this.field.addBlock(this.x + j, this.y + i, new Block(this.getColor(), this.audio))
                }
            }

            this.field.clearTetromino();
        }
        else {

            this.y++;
        }
    }
}


class SquareTetromino extends Tetromino {

    getColor() {

        return "#4beda7"
    }

    getInitialShape() {

        return [
            [1, 1],
            [1, 1]
        ]
    }
}

class ITetromino extends Tetromino {

    getColor() {

        return "#5c4bed"
    }

    getInitialShape() {

        return [
            [1],
            [1],
            [1],
            [1]
        ]
    }
}

class LTetromino1 extends Tetromino {

    getColor() {

        return "#ed4be2"
    }

    getInitialShape() {

        return [
            [1, 0],
            [1, 0],
            [1, 1],
        ]
    }
}

class LTetromino2 extends Tetromino {

    getColor() {

        return "#ed564b"
    }

    getInitialShape() {

        return [
            [0, 1],
            [0, 1],
            [1, 1],
        ]
    }
}

class TTetromino extends Tetromino {

    getColor() {

        return "#ed4b4e"
    }

    getInitialShape() {

        return [
            [0, 1],
            [1, 1],
            [0, 1],
        ]
    }
}

class ZTetromino1 extends Tetromino {

    getColor() {

        return "#eddd4b"
    }

    getInitialShape() {

        return [
            [1, 0],
            [1, 1],
            [0, 1],
        ]
    }
}

class ZTetromino2 extends Tetromino {

    getColor() {

        return "#4bed5e"
    }

    getInitialShape() {

        return [
            [0, 1],
            [1, 1],
            [1, 0],
        ]
    }
}

class Star {

    constructor() {

        this.position = new Vector2(Math.random(), Math.random());
        this.radius = 0.5 + Math.random() * 1;
        this.animationSpeed = (3 + Math.random()) / 4;
        this.time = 0.0;
    }

    step(delta) {
        this.time += delta;
    }

    paint(canvas) {

        let context = canvas.getContext("2d");

        context.beginPath();
        context.arc(this.position.x * canvas.width, this.position.y * canvas.height, 0.25 * (4 + Math.sin(this.animationSpeed * this.time)) * this.radius, 0, 2 * Math.PI);
        context.fillStyle = "#FFFFFF";
        context.shadowBlur = 10;
        context.shadowColor = "#FFFFFF";
        context.fill();
    }
}

class TetrisField {

    static floorPadding = 40;
    static rows = 24;
    static visibleRows = 20;

    constructor() {

        this.glowSince = 0.0;
        this.erasing = false;
        this.field = [];
        this.currentTetromino = null;

        for (let row = 0; row < TetrisField.rows; row++) {

            this.field.push(new Row());
        }
    }

    applyTimeDelta(delta) {

        if (this.erasing) {

            this.glowSince += delta;
        }

        if (this.erasing && this.glowSince > 1) {

            this.erasing = false;


            for (let i = 0; i < 10; i++) {

                let ind = Math.floor(Math.random() * 4);
                let src = audioSourcesList[ind].source;
                let notePath = src[Math.floor(Math.random() * src.length)];

                try {
                    let audio = new Audio(notePath);
                    audio.volume = volume;
                    audio.play()
                } catch (error) {
                    console.error(error);
                }
            }

            let paintableHeight = canvas.height - TetrisField.floorPadding;
            let paintableWidth = paintableHeight * Row.columns / TetrisField.visibleRows;
            let squareSize = paintableHeight / TetrisField.visibleRows;

            let offsetX = (canvas.width / 2) - (paintableWidth / 2);

            for (let rowNum = 0; rowNum < TetrisField.rows; rowNum++) {

                let row = this.field[rowNum];

                if (row.erase) {

                    console.log("Finishing clear");
                    row.finishClear();

                    for (let i = 0; i < 50; i++) {

                        spawnRandomSphere(offsetX, (rowNum - 4) * squareSize, Row.columns * squareSize, squareSize);
                    }

                    continue;
                }
            }

            let sorted = (field) => {

                let foundNonEmpty = false;

                for (let i = 0; i < field.length; i++) {
                    let row = field[i];
                    let rowEmpty = row.isEmpty();

                    if (rowEmpty && foundNonEmpty) {
                        return false;
                    }
                    if (!rowEmpty) {
                        foundNonEmpty = true;
                    }
                }
                return true;
            };

            while (!sorted(this.field)) { // Bubblesort field;

                for (let rowNum = TetrisField.rows - 1; rowNum > 0; rowNum--) {

                    let row = this.field[rowNum];
                    let rowAbove = this.field[rowNum - 1];

                    if (row.isEmpty() && !rowAbove.isEmpty()) {

                        this.field[rowNum] = rowAbove;
                        this.field[rowNum - 1] = row;
                    }
                }
            }

            console.log("Setting new interval");
            clearInterval(stepInterval);
            stepInterval = setInterval(() => {
                if (field != null) {
                    field.step(1);
                }
            }, stepTime);
        }

        this.field.forEach(row => {

            row.applyTimeDelta(delta);
        });


    }

    rotateRight() {

        if (this.currentTetromino == null) {

            return;
        }

        this.currentTetromino.rotateRight();

        if (this.currentTetromino.x + this.currentTetromino.width() >= Row.columns) {
            this.currentTetromino.x = Row.columns - this.currentTetromino.width();
        }
    }

    clearTetromino() {
        this.currentTetromino = null;

        for (let rowNum = 0; rowNum < TetrisField.rows; rowNum++) {

            let row = this.field[rowNum];
            if (row.isFull()) {

                this.erasing = true;
                row.clear();
            }
        }

        if (this.erasing) {

            this.glowSince = 0.0;
            console.log("clearing step interval", stepInterval);
            clearInterval(stepInterval);
        }
    }

    addBlock(x, y, block) {

        this.field[y].addBlock(x, block);
    }

    isBlock(x, y) {

        if (y >= TetrisField.rows) {
            return true;
        }

        return this.field[y].blocks[x] != null;
    }

    moveLeft() {

        if (this.currentTetromino == null) {
            return;
        }

        if (this.currentTetromino.x <= 0) {
            return;
        }

        if (this.currentTetromino.hasLeftStepCollision()) {
            return;
        }

        this.currentTetromino.x--;
    }

    moveRight() {

        if (this.currentTetromino == null) {
            return;
        }

        if (this.currentTetromino.x + this.currentTetromino.width() >= Row.columns) {
            return;
        }

        if (this.currentTetromino.hasRightStepCollision()) {
            return;
        }

        this.currentTetromino.x++;
    }

    step(timeDelta) {

        if (this.currentTetromino != null) {

            this.currentTetromino.step(timeDelta);
        }
        else {

            if (this.dropShapeTimeout == null) {


                this.dropShapeTimeout = setTimeout(() => {

                    this.dropShape();
                    this.dropShapeTimeout = null;
                }, 1000);
            }
        }
    }

    dropShape() {

        let r = Math.random();

        if (r < 1 / 7) {
            // square
            console.log("Spawning Square");
            let x = Math.floor(Math.random() * 11);
            this.currentTetromino = new SquareTetromino(x, 2, this, "red");
        } else if (r < 2 / 7) {
            // T
            console.log("Spawning T");
            let x = Math.floor(Math.random() * 11);
            this.currentTetromino = new TTetromino(x, 1, this, "red");
        } else if (r < 3 / 7) {
            // z1
            console.log("Spawning Z1");
            let x = Math.floor(Math.random() * 11);
            this.currentTetromino = new ZTetromino1(x, 1, this, "red");
        } else if (r < 4 / 7) {
            // z2
            console.log("Spawning Z2");
            let x = Math.floor(Math.random() * 11);
            this.currentTetromino = new ZTetromino2(x, 1, this, "red");
        } else if (r < 5 / 7) {
            // L1
            console.log("Spawning L1");
            let x = Math.floor(Math.random() * 11);
            this.currentTetromino = new LTetromino1(x, 1, this, "red");
        } else if (r < 6 / 7) {
            // L2
            console.log("Spawning L2");
            let x = Math.floor(Math.random() * 11);
            this.currentTetromino = new LTetromino2(x, 1, this, "red");
        } else {
            // I
            console.log("Spawning I");
            let x = Math.floor(Math.random() * 12);
            this.currentTetromino = new ITetromino(x, 0, this, "red");
        }

    }

    paint(canvas) {

        let paintableHeight = canvas.height - TetrisField.floorPadding;
        let paintableWidth = paintableHeight * Row.columns / TetrisField.visibleRows;
        let squareSize = paintableHeight / TetrisField.visibleRows;

        let offsetX = (canvas.width / 2) - (paintableWidth / 2);
        let context = canvas.getContext("2d");

        context.fillStyle = "#363642";
        context.shadowBlur = 20;
        context.shadowColor = "#FFFFFF";

        context.fillRect(offsetX, 0, Row.columns * squareSize, paintableHeight);


        if (this.currentTetromino != null) {

            let shape = this.currentTetromino.getShape();

            context.shadowColor = this.currentTetromino.getColor();
            context.shadowBlur = 10;
            context.fillStyle = this.currentTetromino.getColor();

            for (let i = 0; i < shape.length; i++) {
                for (let j = 0; j < shape[i].length; j++) {

                    if (shape[i][j] == 0) {
                        continue;
                    }

                    context.fillRect(offsetX + ((this.currentTetromino.x + j) * squareSize), (this.currentTetromino.y + i - 4) * squareSize, squareSize, squareSize);

                }
            }
        }

        for (let rowNum = 0; rowNum < TetrisField.rows; rowNum++) {

            let row = this.field[rowNum];

            for (let colNum = 0; colNum < Row.columns; colNum++) {

                let block = row.blocks[colNum];

                if (block == null) {
                    continue;
                }

                //context.shadowColor = null;
                //context.shadowBlur = 0;
                context.shadowColor = block.color;
                context.shadowBlur = 10;
                context.fillStyle = block.color;

                context.fillRect(offsetX + (colNum * squareSize), (rowNum - 4) * squareSize, squareSize, squareSize);
            }
        }

        if (this.erasing && this.glowSince < 1) {

            for (let rowNum = 0; rowNum < TetrisField.rows; rowNum++) {

                let row = this.field[rowNum];

                if (row.erase) {



                    let alpha = this.glowSince * 255;
                    let alphaString = Number(Math.floor(alpha)).toString(16);
                    let pad = "";

                    if (alphaString.length == 1) {
                        pad = "0";
                    }

                    context.fillStyle = "#FFFFFF" + pad + alphaString;
                    context.shadowColor = "#FFFFFF" + pad + alphaString;
                    context.shadowBlur = 10;
                    context.fillRect(offsetX, (rowNum - 4) * squareSize, Row.columns * squareSize, squareSize);
                }
            }
        }
    }
}


var field = new TetrisField();

function run() {

    let now = new Date();
    let delta = now - last;
    delta /= 1000;
    last = now;
    context.clearRect(0, 0, canvas.width, canvas.height);

    stars.forEach(star => {

        star.step(delta);
        star.paint(canvas);
    });

    field.applyTimeDelta(delta);
    field.paint(canvas);

    let newShards = [];

    shards.forEach(shard => {

        if (shard.isDead()) {

            return;
        }

        shard.step(delta);
        shard.paint(canvas);
        newShards.push(shard);
    });

    shards = newShards;
    
    


    window.requestAnimationFrame(run)
}

var stepTime = 1300;
var stepInterval = setInterval(() => {
    if (field != null) {
        field.step(1);
    }
}, stepTime);

var stars = [];

function init() {

    for (let i = 0; i < 300; i ++) {
        stars.push(new Star())
    }

    window.addEventListener("keydown", (ev) => {

        if (ev.key == "ArrowLeft") {

            field.moveLeft();
        }
        else if (ev.key == "ArrowRight") {

            field.moveRight();
        }
        else if (ev.key == "ArrowDown") {

            clearInterval(stepInterval);
            stepInterval = setInterval(() => {
                if (field != null) {
                    field.step(1);
                }
            }, stepTime);
            field.step(0);
        }
        else if (ev.key == "r") {

            field.rotateRight();
        }

    })

    window.addEventListener("wheel", (ev) => {

        if (ev.deltaY > 0) {

            volume = Math.max(0, volume - 0.05);
        }
        else {
            volume = Math.min(1, volume + 0.05);
        }
    });
}

class Vector2 {

    constructor(x, y) {

        this.x = x;
        this.y = y;
    }

    static fromUnitCircle() {

        let theta = Math.random() * 2 * Math.PI;
        return new Vector2(Math.cos(theta), Math.sin(theta));
    }

    normalised() {

        let mag = this.magnitude();
        return new Vector2(this.x / mag, this.y / mag);
    }

    dot(other) {

        return this.x * other.x + this.y * other.y;
    }

    inv() {
        return new Vector2(-this.x, -this.y);
    }

    add(other) {

        return new Vector2(this.x + other.x, this.y + other.y);
    }

    increment(other) {

        this.x += other.x;
        this.y += other.y;
    }

    times(n) {

        return new Vector2(this.x * n, this.y * n);
    }

    magnitude() {

        return Math.sqrt(this.x * this.x + this.y * this.y);
    }

    toString() {

        return "(" + this.x + ", " + this.y + ")";
    }

    orthoUnit() {

        return new Vector2(this.y, - this.x);
    }

    clone() {

        return new Vector2(this.x, this.y);
    }

    distanceTo(other) {

        return (other.add(this.inv()).magnitude());
    }
}


class Shard {

    static baseSpeed = 220;
    static randomSpeedFactor = 110;

    constructor(x, y, playsSound, downwards) {
        this.position = new Vector2(x, y);
        this.speed = Shard.baseSpeed + Math.random() * Shard.randomSpeedFactor;
        this.downwards = downwards;
        this.timeToLive = 0.2 + Math.random() * 0.6;
        this.timeLived = 0.0;
        this.direction = Vector2.fromUnitCircle().times(this.speed);


        this.dead = false;
        this.radius = 2.5 + 10 * Math.random();
        this.playsSound = playsSound;

    }

    step(timeDelta) {

        this.position.increment(this.direction.times(timeDelta));
        this.timeLived += timeDelta;

        if (this.timeLived > this.timeToLive) {

            this.dead = true;
        }
    }

    paint(canvas) {

        if (this.timeLived > this.timeToLive) {
            return;
        }

        let context = canvas.getContext("2d");

        context.fillStyle = "#FFFFFF";
        context.shadowBlur = 15;
        context.shadowColor = "#FFFFFF";
        context.beginPath();
        let radius = (1 - (this.timeLived / this.timeToLive)) * this.radius;

        context.arc(this.position.x, this.position.y, radius, 0, 2 * Math.PI);
        context.fill();
    }

    isDead() {

        return this.dead
    }
}

var shards = [];

function spawnRandomSphere(baseX, baseY, width, height) {

    let x = Math.random() * width;
    let y = Math.random() * height;

    let s = new Shard(baseX + x, baseY + y, false, true);

    shards.push(s);
}


function scaleCanvasToParent() {

    if (canvas == null) {
        console.error("Canvas has not been set yet.");
    }

    canvas.width = canvas.parentNode.clientWidth;
    canvas.height = canvas.parentNode.clientHeight;

}

window.addEventListener("load", () => {
    canvas = document.getElementById("canvas");
    context = canvas.getContext("2d");
    scaleCanvasToParent();

    init();
    run();
})

window.addEventListener("resize", () => {
    scaleCanvasToParent();
})