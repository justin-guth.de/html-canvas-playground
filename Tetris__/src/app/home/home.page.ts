import { Component, OnInit } from '@angular/core';
import { TetrisGame } from './../types/tetris-game';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  private game: TetrisGame;


  constructor() { }

  ngOnInit(): void {

    window.addEventListener("load", () => {

      let canvas = document.getElementById("canvas") as HTMLCanvasElement;

      this.game = new TetrisGame(canvas);

      this.game.run();
    })

  }



}
