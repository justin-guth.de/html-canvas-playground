import { GameObject } from "./GameObject";

export class Sphere extends GameObject {

  private radius: number = 10;

  public paint() {

    let context:CanvasRenderingContext2D = this.getGame().getContext();
    let position = this.getPosition();
    context.beginPath();
    context.arc(position.getX(), position.getY(), this.radius, 0, 2 * Math.PI);
    context.fillStyle = "red";
    context.fill();
  }

}
