export class Vector2 {

  private x: number;
  private y: number;

  constructor(x, y) {

    this.x = x;
    this.y = y;
  }

  public getX() {return this.x};
  public getY() {return this.y};

  public static fromUnitCircle(): Vector2 {

    let theta = Math.random() * 2 * Math.PI;
    return new Vector2(Math.cos(theta), Math.sin(theta));
  }

  public normalised(): Vector2 {

    let mag = this.magnitude();
    return new Vector2(this.x / mag, this.y / mag);
  }

  public dot(other): number {

    return this.x * other.x + this.y * other.y;
  }

  public inv(): Vector2 {
    return new Vector2(-this.x, -this.y);
  }

  public add(other): Vector2 {

    return new Vector2(this.x + other.x, this.y + other.y);
  }

  public increment(other): Vector2 {

    this.x += other.x;
    this.y += other.y;
    return this;
  }

  public times(n): Vector2 {

    return new Vector2(this.x * n, this.y * n);
  }

  public magnitude(): number {

    return Math.sqrt(this.x * this.x + this.y * this.y);
  }

  public toString(): string {

    return "(" + this.x + ", " + this.y + ")";
  }

  public orthoUnit(): Vector2 {

    return new Vector2(this.y, - this.x);
  }

  public clone(): Vector2 {

    return new Vector2(this.x, this.y);
  }

  public distanceTo(other): number {

    return (other.add(this.inv()).magnitude());
  }
}
