import { Vector2 } from './vector2';
import { GameInterface } from "./GameInterface";
import { GameObjectInterface } from "./GameObjectInterface";
import { GameObjectModuleInterface } from "./GameObjectModuleInterface";

export class GameObject implements GameObjectInterface {

  private name: string;
  private modules: GameObjectModuleInterface[] = [];
  private game: GameInterface;

  private position: Vector2 = new Vector2(0,0);

  constructor(game: GameInterface) {

    this.game = game;
    this.onInit();
  }

  onInit() { }

  public getName(): string {
    return this.name;
  }

  public setName(name: string): void {
    this.name = name;
  }

  public getGame(): GameInterface {

    return this.game;
  }

  public step(): void {

    this.modules.forEach((element) => {

      element.process();
    });
  }

  public addComponent(module: GameObjectModuleInterface): void {

    this.modules.push(module);
  }

  public setPosition(pos: Vector2): void {
    this.position = pos;
  }

  public getPosition(): Vector2 {
    return this.position;
  }

  public paint(): void {

  }
}
