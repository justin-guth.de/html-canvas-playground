export class Physics {
    static applyGravity(momentumY: any, delta: any) {

        return momentumY + 9.81 * delta;
    }
};