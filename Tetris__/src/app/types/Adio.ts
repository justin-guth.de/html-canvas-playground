var volume = 0.5;

var audioSourcesList = [
    {
        source: [

            "./Akk1 C5.wav",
            "./Akk1 C6.wav",
            "./Akk1 E5.wav",
            "./Akk1 E6.wav",
            "./Akk1 F5.wav",
            "./Akk1 G4.wav",
            "./Akk1 G5.wav",
            "./Akk1 G6.wav"
        ],
        color: "#99BBFF"
    },
    {
        source: [

            "./Akk2 A4.wav",
            "./Akk2 A5.wav",
            "./Akk2 D5.wav",
            "./Akk2 D6.wav",
            "./Akk2 F5.wav",
            "./Akk2 F6.wav",
            "./Akk2 G5.wav",
            "./Akk2 A6.wav"
        ],
        color: "#ffc899"
    },
    {
        source: [

            "./Akk3 A4.wav",
            "./Akk3 A5.wav",
            "./Akk3 B6.wav",
            "./Akk3 C5.wav",
            "./Akk3 C6.wav",
            "./Akk3 E5.wav",
            "./Akk3 E6.wav",
            "./Akk3 B5.wav"
        ],
        color: "#99ffa6"
    },
    {
        source: [

            "./Akk4 A6.wav",
            "./Akk4 B4.wav",
            "./Akk4 B5.wav",
            "./Akk4 D5.wav",
            "./Akk4 D6.wav",
            "./Akk4 G5.wav",
            "./Akk4 G6.wav",
            "./Akk4 G4.wav"
        ],
        color: "#ff9999"
    }

];

var audioSourceIndex = 0;

function playRandomNote() {

    let source = audioSourcesList[audioSourceIndex].source;
    let notePath = source[Math.floor(Math.random() * source.length)];

    try {
        let audio = new Audio(notePath);
        audio.volume = volume;
        audio.play()
    } catch (error) {
        console.error(error);
    }
}

