import { GameObjectInterface } from './../../../../Tetris_/GameObjectInterface';
import { Dimensions } from "./Dimensions";
import { GameInterface } from "./GameInterface";

export abstract class Game implements GameInterface {

  private canvas: HTMLCanvasElement;
  private context: CanvasRenderingContext2D;

  private lastFrame: Date = new Date();
  private lastDelta: number = 0;
  private gameObjects: GameObjectInterface[] = [];

  constructor(canvas: HTMLCanvasElement) {

    this.canvas = canvas;
    this.context = canvas.getContext("2d") as CanvasRenderingContext2D;

    this.init();
  }

  public addGameObject(object: GameObjectInterface) {

    this.gameObjects.push(object);
  }

  public run() {

    let now: Date = new Date();
    this.lastDelta = (now.getTime() - this.lastFrame.getTime()) / 1000;
    this.lastFrame = now;
    this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);

    this.onUpdate();

    this.gameObjects.forEach((gameObject: GameObjectInterface) => {

      gameObject.paint();
    })


    window.requestAnimationFrame(() => {this.run();});
  }

  public getContext() : CanvasRenderingContext2D {

    return this.context;
  }

  public getCanvasDimensions(): Dimensions {

    return {
      width: this.canvas.width,
      height: this.canvas.height,
    };
  }

  public getTimeDelta(): number {

    return this.lastDelta;
  }

  protected  onUpdate(): void {}
  protected  onInit(): void {}



  private init(): void {

    console.log("Initialising Game");

    window.addEventListener("click", (ev) => {


    })

    window.addEventListener("wheel", (ev) => {


    });

    window.addEventListener("resize", () => {

      this.scaleCanvasToParent();
    })

    this.scaleCanvasToParent();
    this.onInit();
  }


  private scaleCanvasToParent(): void {

    if (this.canvas == null) {
      console.error("Canvas has not been set yet.");
    }

    this.canvas.width = (this.canvas.parentNode as HTMLDivElement).clientWidth;
    this.canvas.height = (this.canvas.parentNode as HTMLDivElement).clientHeight;
  }
}
