import { Dimensions } from "./Dimensions";

export interface GameInterface {

  getContext() : CanvasRenderingContext2D;
  getCanvasDimensions(): Dimensions;

}
