import { GameObjectInterface } from "./GameObjectInterface";
import { GameObjectModuleInterface } from "./GameObjectModuleInterface";

export abstract class GameObjectModule implements GameObjectModuleInterface {

    private parent: GameObjectInterface;

    constructor(parent: GameObjectInterface) {

        this.parent = parent;
    }

    public getParent() : GameObjectInterface {

      return this.parent;
    }

    public abstract process(): void;
}
