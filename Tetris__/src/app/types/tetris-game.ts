import { Game } from "./Game";
import { Sphere } from "./sphere";
import { Vector2 } from "./vector2";

export class TetrisGame extends Game {

  public onInit() {

    let sphere: Sphere = new Sphere(this);
    this.addGameObject(sphere);
    sphere.setPosition(new Vector2(100,100));
  }

  public onUpdate() {



  }
}
