var canvas = null;
var context = null;

var last = new Date();
var lastSpawnTime = 0;

var stars = [];
var shards = [];
var fizz = [];


var volume = 0.5;

var audioSourcesA = [

    "./Akk1 C5.mp3",
    "./Akk1 C6.mp3",
    "./Akk1 E5.mp3",
    "./Akk1 E6.mp3",
    "./Akk1 F5.mp3",
    "./Akk1 G4.mp3",
    "./Akk1 G5.mp3",
    "./Akk1 G6.mp3"
];


var audioSourcesB = [

    "./Akk2 A4.mp3",
    "./Akk2 A5.mp3",
    "./Akk2 D5.mp3",
    "./Akk2 D6.mp3",
    "./Akk2 F5.mp3",
    "./Akk2 F6.mp3",
    "./Akk2 G5.mp3",
    "./Akk2 A6.mp3"
];

var audioSourcesC = [

    "./Akk3 A4.mp3",
    "./Akk3 A5.mp3",
    "./Akk3 B6.mp3",
    "./Akk3 C5.mp3",
    "./Akk3 C6.mp3",
    "./Akk3 E5.mp3",
    "./Akk3 E6.mp3",
    "./Akk3 B5.mp3"
];

var audioSourcesD = [

    "./Akk4 A6.mp3",
    "./Akk4 B4.mp3",
    "./Akk4 B5.mp3",
    "./Akk4 D5.mp3",
    "./Akk4 D6.mp3",
    "./Akk4 G5.mp3",
    "./Akk4 G6.mp3",
    "./Akk4 G4.mp3"
];

var audioSourcesList = [
    {
        source: audioSourcesA,
        color: "#99BBFF"
    },
    {
        source: audioSourcesB,
        color: "#ffc899"
    },
    {
        source: audioSourcesC,
        color: "#99ffa6"
    },
    {
        source: audioSourcesD,
        color: "#ff9999"
    }

];

var audioSourceIndex = 0;

function applyGravity(momentumY, delta) {

    return momentumY + 9.81 * delta * 50;
};


function playRandomNote() {

    let source = audioSourcesList[audioSourceIndex].source;
    let notePath = source[Math.floor(Math.random() * source.length)];

    try {
        let audio = new Audio(notePath);
        audio.volume = volume;
        audio.play()
    } catch (error) {
        console.error(error);
    }
}

class Star {

    static speed = 280;
    static baseShardSpawn = 5;
    static randomShardSpawnFactor = 6;

    constructor(x, y, downwards) {
        this.x = x;
        this.y = y;
        if (downwards) {

            this.movementY = Star.speed;
        } else {
            this.movementY = - Star.speed;
        }

        this.dead = false;
        this.baseShadowBlurRadius = 25;
        this.shadowBlurRandomScale = 5;
        this.shadowBlurRadiusOffset = 0;
        this.timeLived = 0.0;
        this.blurRadiusFrequency = (5.0 + Math.random()) / 2;
        this.radius = 10 + (4 * Math.random()) - 2;
        this.playsSound = Math.random() < 0.3;
        this.downwards = downwards;
    }

    step(timeDelta) {

        this.timeLived += timeDelta;

        this.shadowBlurRadiusOffset = Math.sin(Math.PI * this.timeLived * this.blurRadiusFrequency) * this.shadowBlurRandomScale;

        if (this.downwards) {

            this.movementY = applyGravity(this.movementY, timeDelta);
        }
        else {
            this.movementY = -applyGravity(-this.movementY, timeDelta);
        }
        this.y += this.movementY * timeDelta;

        if (this.y > canvas.height / 2 && this.downwards) {

            if (this.playsSound) {

                playRandomNote();
            }

            this.dead = true;

            for (let i = 0; i < Star.baseShardSpawn + Math.floor((Math.random() * Star.randomShardSpawnFactor)); i++) {

                shards.push(new Shard(this.x, this.y, this.playsSound, this.downwards));
            }
        }
        else if (this.y < canvas.height / 2 && !this.downwards) {

            if (this.playsSound) {

                playRandomNote();
            }

            this.dead = true;

            for (let i = 0; i < Star.baseShardSpawn + Math.floor((Math.random() * Star.randomShardSpawnFactor)); i++) {

                shards.push(new Shard(this.x, this.y, this.playsSound, this.downwards));
            }
        }
    }

    getGlowRadius() {
        return this.baseShadowBlurRadius + this.shadowBlurRadiusOffset;
    }

    isDead() {

        return this.dead;
    }
}

class Shard {

    static baseSpeed = 220;
    static randomSpeedFactor = 110;

    constructor(x, y, playsSound, downwards) {
        this.x = x;
        this.y = y;
        this.speed = Shard.baseSpeed + Math.random() * Shard.randomSpeedFactor;
        this.downwards = downwards;
        this.movementX = Math.cos((Math.random() * Math.PI)) * this.speed;
        this.timeToLive = 0.6 + Math.random() * 1;
        this.timeLived = 0.0;

        if (this.downwards) {

            this.movementY = -Math.sin((Math.random() * Math.PI)) * this.speed;
        } else {

            this.movementY = Math.sin((Math.random() * Math.PI)) * this.speed;
        }

        this.dead = false;
        this.radius = 2.5 + 1.5 * Math.random();
        this.playsSound = playsSound;

    }

    step(timeDelta) {

        this.x += this.movementX * timeDelta;
        this.y += this.movementY * timeDelta;
        this.timeLived += timeDelta;

        if (this.y > canvas.height + 20 && this.downwards || this.y < 0 - 20 && !this.downwards || this.timeLived > this.timeToLive) {

            this.dead = true;
        }
    }

    isDead() {

        return this.dead
    }
}

class Fizz {

    static baseSpeed = 0;
    static randomSpeedFactor = 5;

    constructor(x, y, playsSound, downwards) {
        this.x = x;
        this.y = y;
        this.downwards = downwards;

        this.speed = Fizz.baseSpeed + Math.random() * Fizz.randomSpeedFactor;
        this.movementX = Math.cos((Math.random() * Math.PI)) * this.speed / 2;
        if (this.downwards) {

            this.movementY = -Math.sin((Math.random() * Math.PI)) * this.speed;
        }else {
            this.movementY = Math.sin((Math.random() * Math.PI)) * this.speed;
        }
        this.dead = false;
        this.timeLived = 0.0;
        this.timeToLive = 0.3 * (1 + 2 * Math.random());
        this.radius = 0.1 + 2 * Math.random();
        this.playsSound = playsSound;

    }

    step(timeDelta) {

        this.timeLived += timeDelta;

        this.x += this.movementX * timeDelta;
        this.movementY = applyGravity(this.movementY, timeDelta);
        this.y += this.movementY * timeDelta;

        if (this.y > canvas.height + 20) {

            this.dead = true;
        }
    }

    isDead() {

        return this.timeLived > this.timeToLive;
    }
}


function randomStarDrop() {

    if (Math.random() < 0.06) {

        starDrop();
    }
}

function starDrop() {

    let x = Math.random() * canvas.width;

    stars.push(new Star(x, -10, true));
    stars.push(new Star(x, canvas.height + 10, false));
}

function run() {

    let now = new Date();
    let delta = now - last;
    delta /= 1000;
    last = now;
    context.clearRect(0, 0, canvas.width, canvas.height);

    let newStars = [];

    context.fillStyle = "#DDEEFF";

    context.shadowColor = "#DDFFFF";
    context.shadowOffsetX = 0;
    context.shadowOffsetY = 0;

    stars.forEach((s, i) => {

        s.step(delta);

        context.shadowBlur = s.getGlowRadius();
        context.beginPath();
        context.arc(s.x, s.y, s.radius, 0, Math.PI * 2);

        if (s.playsSound) {
            context.fillStyle = audioSourcesList[audioSourceIndex].color;
            context.shadowColor = audioSourcesList[audioSourceIndex].color;

        } else {
            context.fillStyle = "#DDEEFF";
            context.shadowColor = "#DDFFFF";

        }
        context.fill();

        if (Math.random() < 0.5) {

            fizz.push(new Fizz(s.x + (16 * Math.random()) - 8, s.y, s.playsSound, s.downwards));

        }



        if (!s.isDead()) {

            newStars.push(s);
        }
    });

    stars = newStars;

    context.shadowBlur = 5;

    let newShards = [];


    shards.forEach((sh, i) => {



        sh.step(delta);
        context.beginPath();
        context.arc(sh.x, sh.y, sh.radius * Math.max(0, sh.timeToLive - sh.timeLived), 0, Math.PI * 2);

        if (sh.playsSound) {
            context.fillStyle = audioSourcesList[audioSourceIndex].color;
            context.shadowColor = audioSourcesList[audioSourceIndex].color;
        } else {
            context.fillStyle = "#DDEEFF";
            context.shadowColor = "#DDFFFF";
        }

        context.fill();

        if (!sh.isDead()) {

            newShards.push(sh);
        }
    });

    let newFizz = [];

    context.shadowBlur = 2;

    fizz.forEach((f) => {

        f.step(delta);
        context.beginPath();
        context.arc(f.x, f.y, f.radius, 0, Math.PI * 2);

        if (f.playsSound) {
            context.fillStyle = audioSourcesList[audioSourceIndex].color;
            context.shadowColor = audioSourcesList[audioSourceIndex].color;
        } else {
            context.fillStyle = "#DDEEFF";
            context.shadowColor = "#DDFFFF";
        }

        context.fill();

        if (!f.isDead()) {

            newFizz.push(f);
        }
    });

    fizz = newFizz;

    shards = newShards;



    window.requestAnimationFrame(run)
}

function init() {

    setInterval(randomStarDrop, 10);
    setInterval(starDrop, 300);

    window.addEventListener("click", (ev) => {

        audioSourceIndex++;
        audioSourceIndex %= audioSourcesList.length;
        //stars.push(new Star(ev.clientX, -10));
    })

    window.addEventListener("wheel", (ev) => {

        if (ev.deltaY > 0) {

            volume = Math.max(0, volume - 0.05);
        }
        else {
            volume = Math.min(1, volume + 0.05);
        }
    });
}


function scaleCanvasToParent() {

    if (canvas == null) {
        console.error("Canvas has not been set yet.");
    }

    canvas.width = canvas.parentNode.clientWidth;
    canvas.height = canvas.parentNode.clientHeight;

}

window.addEventListener("load", () => {
    canvas = document.getElementById("canvas");
    context = canvas.getContext("2d");
    scaleCanvasToParent();

    init();
    run();
})

window.addEventListener("resize", () => {
    scaleCanvasToParent();
})